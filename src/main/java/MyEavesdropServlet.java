import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.security.SecureRandom;
import java.util.*;

/**
 * Created by Sara on 2/20/2015.
 *
 * cookie: myeavesdrop, unique session id
 * sessionMap: unique session id, ArrayList<urls>
 * usernameMap: username, unique session id
 *
 *     start session,
 *          check if session exists - need to find unique session id
 *          if it doesn't, then create new session id and cookie
 *     check cookies, look through cookies
 *          if cookie name is myeavesdrop
 *              check if session id is in sessionMap, if it is then get url list
 *              if not then that session is closed
 *
 *
 *   parse query parameters
 *      check for user trying to start a session
 *          check if session exists - get unique session id from usernameMap
 *          if session exists, respond with error message
 *          if session does not exists, create cookie session
 *      check for user making requests
 *          check if request has a myeavesdrop session cookie
 *          if it does then add url request to visited urls, make request
 *          if it doesn't then just make request
 *      check if user trying to end a session
 *          if user does not exist, respond with error message
 *          if user exists, delete the map values for that user and delete the cookie?
 *
 *
 */
public class MyEavesdropServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    //hashmap for users/cookies and visited urls
    //user identifier, visited urls
    Map<String, ArrayList<String>> sessionMap = new HashMap<String, ArrayList<String>>();
    Map<String, String> usernameMap = new HashMap<String, String>();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        /*response.getWriter().println("Hello world. What's up MyEavesdropServlet?");
        response.getWriter().println("Request URL:" + request.getRequestURL()); // http://localhost:8080/myeavesdrop
        response.getWriter().println("Request URI:" + request.getRequestURI()); // /myeavesdrop
        response.getWriter().println("Servlet Path:" + request.getServletPath()); // /myeavesdrop
        response.getWriter().println("Request Query: " + request.getQueryString());*/

        // Response is URLEncoded
        //response.getWriter().println("Query String:" + request.getQueryString()); //gives the query string

        // How to decode the URL encoded value?
        String queryString = URLDecoder.decode(request.getQueryString(), "UTF-8");
        //gives the whole query string, need to parse this and test what's in it
        /*response.getWriter().println("Decoded Query String:" + queryString);
        response.getWriter().println("***********");*/


        //strip query string by &, then go through all the param,val pairs and strip by =
        Map<String, String> parameterMap = getQueryMap(request.getQueryString());
        Set<String> keys = parameterMap.keySet();
        /*for (String key : keys) {
            System.out.println("Parameter=" + key);
            System.out.println("Value=" + parameterMap.get(key));
        }*/

        String username = new String();
        String session = new String();
        String type = new String();
        String project = new String();
        String year = new String();


        /*
        * check for user trying to start a session
        *    check if session exists - get unique session id from usernameMap
        *    if session exists, respond with error message
        *    if session does not exists, create cookie session
        */
        if(parameterMap.containsKey("username")) {
            username = parameterMap.get("username");
            if(username.equals(" ") || username == null) {
                //not allowed
                response.getWriter().println("Disallowed value specified for parameter username " + username);
            }
            else {
                if(parameterMap.containsKey("session")) {
                    session = parameterMap.get("session");
                    if(session.equals("start")) {
                        //if session for this user has already been started, throw error
                        // Error: Session already active
                        if(usernameMap.containsKey(username)) {
                            //System.out.println("Session active for username: " + username);
                            response.getWriter().println("Error: Session already active");
                        }
                        //add user to sessionMap
                        else {
                            //make cookie
                            //String usessionID = getSessionIDFromCookie(request, response);
                            //System.out.println("usession is empty");
                            String usessionID = createSessionIDAndCookie(request, response);
                            usernameMap.put(username, usessionID);
                        }
                    }
                    else if(session.equals("end")) {
                        //if session for this user has already been ended, throw error Error: Session does not exist
                        if(!usernameMap.containsKey(username)) {
                            response.getWriter().println("Error: Session does not exist");
                        }
                        //remove username from sessionMap
                        else {
                            String remsessionID = usernameMap.get(username);
                            sessionMap.remove(remsessionID);
                            usernameMap.remove(username);
                            /*response.getWriter().println("Removed user: " + username);
                            response.getWriter().println("Removed session id: " + remsessionID);*/
                        }
                    }
                    else if(!session.equals("start") || !session.equals("end")) {
                        response.getWriter().println("Disallowed value specified for parameter session " + session);
                    }
                }
            }
        }

        // keep track of user session with cookies
        // request url page data - filter to just get the urls

        if(parameterMap.containsKey("type") && parameterMap.containsKey("project")) {
            type = parameterMap.get("type");
            project = parameterMap.get("project");
            if(type.equals("meetings")) {
                //allowed values are only those that appear as project names on
                // http://eavesdrop.openstack.org/meetings/
                //httprequest for the link on that page and parse for valid names
                String projectURLstr = "http://eavesdrop.openstack.org/" + type + "/";
                boolean validProject = validateProject(request, response, projectURLstr, project);
                if(!validProject) {
                    response.getWriter().println("Error: not a valid URL");
                }
                else {
                    if(parameterMap.containsKey("year")) {
                        year = parameterMap.get("year");
                        int yearint = Integer.parseInt(year);
                        if(yearint < 2010 || yearint > 2015) {
                            //not allowed
                            response.getWriter().println("Disallowed value specified for parameter year");
                        }
                        else {
                            //print visited urls if necessary
                            printVisitedURLS(request, response);

                            //get resource data
                            String url = "http://eavesdrop.openstack.org/" + type + "/" + project + "/" + year +"/";
                            getResourceData(request, response, url);

                            String userQuery = request.getRequestURL() + "?" + request.getQueryString();

                            //add url to visited urls if necessary
                            String usessionID = getSessionIDFromCookie(request,response);
                            if(!usessionID.isEmpty()) {
                                //in an active session, add url to visited urls
                                ArrayList<String> currVisitedQueries = sessionMap.get(usessionID);
                                currVisitedQueries.add(userQuery);
                                sessionMap.put(usessionID, currVisitedQueries);
                            }

                        }
                    }
                }
            }
            if(type.equals("irclogs")) {
                //allowed values are only those that appear as project names on
                // http://eavesdrop.openstack.org/irclogs/
                //httprequest for the link on that page and parse for valid names
                String projectURLstr = "http://eavesdrop.openstack.org/" + type + "/";
                boolean validProject = validateProject(request, response, projectURLstr, project);
                if(!validProject) {
                    response.getWriter().println("Error: not a valid URL");
                }
                else {
                    //print visited urls if necessary
                    printVisitedURLS(request,response);

                    //query the irclogs/project page for data
                    String url = "http://eavesdrop.openstack.org/" + type + "/" + project + "/";
                    getResourceData(request, response, url);

                    //add url to visited urls if necessary
                    String usessionID = getSessionIDFromCookie(request,response);
                    String userQuery = request.getRequestURL() + "?" + request.getQueryString();

                    if(!usessionID.isEmpty()) {
                        //in an active session, add url to visited urls
                        ArrayList<String> currVisitedQueries = sessionMap.get(usessionID);
                        currVisitedQueries.add(userQuery);
                        sessionMap.put(usessionID, currVisitedQueries);
                    }
                }
            }
            else if(!type.equals("meetings") && !type.equals("irclogs")) {
                response.getWriter().println("Disallowed value specified for parameter type");
            }
        }

    }

    private boolean validateProject(HttpServletRequest request, HttpServletResponse response,
                                    String projectURLstr, String project) throws IOException {
        boolean validType = false;
        URL url = null;
        try {
            url = new URL(projectURLstr);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        assert url != null;

        String content = null;
        URLConnection connection = null;
        try {
            connection =  url.openConnection();
            Scanner scanner = new Scanner(connection.getInputStream());
            scanner.useDelimiter("\\Z");
            content = scanner.next();
        }catch ( Exception ex ) {
            ex.printStackTrace();
        }

        //filter this result and only get the links and print to browser
        HTMLLinkExtractor linkExtractor = new HTMLLinkExtractor();
        Vector<HTMLLinkExtractor.HtmlLink> linklist = linkExtractor.grabHTMLLinks(content);
        //response.getWriter().println("URL Data");
        boolean afterParent = false;
        for(HTMLLinkExtractor.HtmlLink link : linklist) {
            if(afterParent) {
                String onlyLink = link.getLink().replaceAll("/","");
                String urlLink = projectURLstr + onlyLink;
                if(onlyLink.equals(project)) {
                    return validType = true;
                }
                //System.out.println("link: " + onlyLink);
                //System.out.println("urlLink: " + urlLink);
                //response.getWriter().println(urlLink);
            }
            if(link.linkText.equals("Parent Directory")) {
                afterParent = true;
            }
        }

        return validType;
    }

    SessionIdentifierGenerator sessionIDGen = new SessionIdentifierGenerator();
    public final class SessionIdentifierGenerator {
        private SecureRandom random = new SecureRandom();

        public String nextSessionId() {
            return new BigInteger(130, random).toString(32);
        }
    }

    //strip query string by &, then go through all the param,val pairs and strip by =
    public static Map<String, String> getQueryMap(String query)
    {
        String[] parameters = query.split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String param : parameters) {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            map.put(name, value);
        }
        return map;
    }


    public void getResourceData(HttpServletRequest request, HttpServletResponse response, String urlstr) throws IOException{
        URL url = null;
        try {
            url = new URL(urlstr);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        assert url != null;


        String content = null;
        URLConnection connection = null;
        try {
            connection =  url.openConnection();
            Scanner scanner = new Scanner(connection.getInputStream());
            scanner.useDelimiter("\\Z");
            content = scanner.next();
        }catch ( Exception ex ) {
            ex.printStackTrace();
        }


        //filter this result and only get the links and print to browser
        HTMLLinkExtractor linkExtractor = new HTMLLinkExtractor();
        Vector<HTMLLinkExtractor.HtmlLink> linklist = linkExtractor.grabHTMLLinks(content);
        response.getWriter().println("URL Data");
        boolean afterParent = false;
        for(HTMLLinkExtractor.HtmlLink link : linklist) {
            if(afterParent) {
                String onlyLink = link.getLink();
                String urlLink = urlstr + onlyLink;
                //System.out.println("link: " + onlyLink);
                //System.out.println("urlLink: " + urlLink);
                response.getWriter().println(urlLink);
            }
            if(link.linkText.equals("Parent Directory")) {
                afterParent = true;
            }
        }
    }


    /*
     * If this request contains cookies, go through the cookies and find any myeavesdrop cookies
     * check if cookie value (session id) is in sessionMap, if it is then return that session id
     * if there are no myeavesdrop cookies then it will create a new cookie and return the new session id
     */
    public String createSessionIDAndCookie(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String  sessionID = new String();
        Cookie cookies [] = request.getCookies();
        boolean friend = false;

        if (!friend) {
            String randsessionID = sessionIDGen.nextSessionId();
            Cookie cookie = new Cookie("myeavesdrop",randsessionID);
            sessionID = randsessionID;
            sessionMap.put(sessionID, new ArrayList<String>());
            //cookie.setDomain("localhost");
            //cookie.setPath(request.getServletPath());
            //response.getWriter().println("getServletPath for new cookie: " + request.getServletPath());
            cookie.setMaxAge(1000);
            response.addCookie(cookie);
            //response.getWriter().println("Hello, stranger.");
        }
        return sessionID;
    }

    public String getSessionIDFromCookie(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String  sessionID = new String();
        Cookie cookies [] = request.getCookies();
        boolean friend = false;
        for(int i=0; cookies != null && i<cookies.length; i++) {
            Cookie ck = cookies[i];
            String cookieName = ck.getName();
            //response.getWriter().println("Cookie name: " + cookieName);
            String cookieValue = ck.getValue();
            //response.getWriter().println("Cookie value: " + cookieValue);

            if ((cookieName != null && cookieName.equals("myeavesdrop"))
                    && cookieValue != null && sessionMap.containsKey(cookieValue)) {
                /*response.getWriter().println("Hello, friend.");
                response.getWriter().println("Domain:" + ck.getDomain());
                response.getWriter().println("Path:" + ck.getPath());*/
                sessionID = cookieValue;
                friend = true;
            }
        }
        return sessionID;
    }

    public void printVisitedURLS(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String visitedURLS = new String();

        String user = new String();
        String currentSession = getSessionIDFromCookie(request,response);
        for(Map.Entry<String, String> entry : usernameMap.entrySet()) {
            if(entry.getValue().equals(currentSession)) {
                user = entry.getKey();
            }
        }

        if(!user.equals(null) && !user.equals(" ") && !user.isEmpty()) {
            response.getWriter().println("Visited URLs");
            //response.getWriter().println("user: " + user);
            ArrayList<String> sessionList = sessionMap.get(currentSession);
            for(String url : sessionList) {
                response.getWriter().println(url);
            }
            response.getWriter().println(); //newline inbetween visited urls and response data
        }
        /*response.getWriter().println("User: " + user);
        response.getWriter().println("User session id: " + currentSession);*/

    }
}
